<?php

/**
 * peach.php
 *
 * Copyright (c) 2018 Plaas
 * 
 * @author     Jonathan Wagener <jonathan@plaas.co>
 * @version    1.0.0
 * @date       16/03/2018
 *
 * @link       https://plaas.co
 */

class PeachValidationModuleFrontController extends ModuleFrontController
{
    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        $cart = $this->context->cart;

        //print '<pre>';
        //print_r($cart);
        //print '</pre>';

        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'peach') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die($this->module->l('This payment method is not available.', 'validation'));
        }

        $this->context->smarty->assign([
            'params' => $_REQUEST,
        ]);

        $validate_url = Configuration::get('PEACH_LIVE_VALIDATE_URL');
        if (Configuration::get('PEACH_MODE') == 'test') {
            $validate_url = Configuration::get('PEACH_TEST_VALIDATE_URL');
        }

        $url = str_replace('{checkout_id}', $_GET['checkout_id'], $validate_url);
        //$url .= "?authentication.userId=". Configuration::get('PEACH_USER_ID');
        //$url .= "&authentication.password=". Configuration::get('PEACH_PASSWORD');
        $url .= "?entityId=". Configuration::get('PEACH_ENTITY_ID');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('Authorization:Bearer %s', Configuration::get('PEACH_ACCESS_TOKEN'))));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $responseData = json_decode(curl_exec($ch));

        if(curl_errno($ch)) {
            print '<pre>Error: ';
            print_r(curl_error($ch));
            print '</pre>';
        }

        curl_close($ch);

        $success_codes = array(
            '000.000.000',
            '000.100.110',
            '000.100.111',
            '000.100.112',
        );

        //die;

        if (!in_array($responseData->result->code, $success_codes)) {
            Tools::redirect('index.php?controller=order&step=1');
            die;
        }

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        $currency = $this->context->currency;
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $mailVars = array();

        $this->module->validateOrder($cart->id, 2, $total, $this->module->displayName, NULL, $mailVars, (int)$currency->id, false, $customer->secure_key);
        Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);
    }
}
