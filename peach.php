<?php

/**
 * peach.php
 *
 * Copyright (c) 2018 Plaas
 * 
 * @author     Jonathan Wagener <jonathan@plaas.co>
 * @version    1.0.0
 * @date       16/03/2018
 *
 * @link       https://plaas.co
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if( !defined( '_PS_VERSION_' ) )
    exit;

class Peach extends PaymentModule
{
    const LEFT_COLUMN = 0;
    const RIGHT_COLUMN = 1;
    const FOOTER = 2;
    const DISABLE = -1;
    
    public function __construct()
    {
        $this->name = 'peach';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);  
        $this->currencies = true;
        $this->currencies_mode = 'radio';
        $this->display = 'view';
        $this->bootstrap = true;
        $this->controllers = array('validation');
       
        $this->author  = 'Plaas';
        //$this->page = basename(__FILE__, '.php');

        parent::__construct();

        $this->displayName = $this->l('Peach');
        $this->description = $this->l('Accept payments by credit card quickly and securely with Peach.');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');
        $this->module_link = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
    }

    public function install()
    {
        if ( !parent::install()
            OR !$this->registerHook('paymentOptions') 
            OR !$this->registerHook('paymentReturn') 
            OR !Configuration::updateValue('PEACH_USER_ID', '') 
            OR !Configuration::updateValue('PEACH_PASSWORD', '') 
            OR !Configuration::updateValue('PEACH_ENTITY_ID', '') 
            OR !Configuration::updateValue('PEACH_ACCESS_TOKEN', '') 
            OR !Configuration::updateValue('PEACH_MODE', 'test') 
            OR !Configuration::updateValue('PEACH_TEST_PREPARE_URL', 'https://test.oppwa.com/v1/checkouts') 
            OR !Configuration::updateValue('PEACH_LIVE_PREPARE_URL', 'https://oppwa.com/v1/checkouts') 
            OR !Configuration::updateValue('PEACH_TEST_CHECKOUT_URL', 'https://test.oppwa.com/v1/paymentWidgets.js?checkoutId={checkout_id}') 
            OR !Configuration::updateValue('PEACH_LIVE_CHECKOUT_URL', 'https://oppwa.com/v1/paymentWidgets.js?checkoutId={checkout_id}') 
            OR !Configuration::updateValue('PEACH_TEST_VALIDATE_URL', 'https://test.oppwa.com/v1/checkouts/{checkout_id}/payment') 
            OR !Configuration::updateValue('PEACH_LIVE_VALIDATE_URL', 'https://oppwa.com/v1/checkouts/{checkout_id}/payment') 
            OR !Configuration::updateValue('PEACH_BRANDS', 'VISA MASTER AMEX') 
            OR !Configuration::updateValue('PEACH_PAYMENT_TYPE', '') 
            OR !Configuration::updateValue('PEACH_PAYNOW_TEXT', 'Pay Now With Peach') 
            OR !Configuration::updateValue('PEACH_PAYNOW_LOGO', 'on') 
            OR !Configuration::updateValue('PEACH_PAYNOW_ALIGN', 'right'))
        {            
            return false;
        }
            
        return true;
    }

    public function uninstall()
    {
        return (parent::uninstall() 
            AND Configuration::deleteByName('PEACH_USER_ID') 
            AND Configuration::deleteByName('PEACH_PASSWORD') 
            AND Configuration::deleteByName('PEACH_ENTITY_ID') 
            AND Configuration::deleteByName('PEACH_ACCESS_TOKEN') 
            AND Configuration::deleteByName('PEACH_MODE') 
            AND Configuration::deleteByName('PEACH_TEST_PREPARE_URL') 
            AND Configuration::deleteByName('PEACH_LIVE_PREPARE_URL') 
            AND Configuration::deleteByName('PEACH_TEST_CHECKOUT_URL') 
            AND Configuration::deleteByName('PEACH_LIVE_CHECKOUT_URL') 
            AND Configuration::deleteByName('PEACH_TEST_VALIDATE_URL') 
            AND Configuration::deleteByName('PEACH_LIVE_VALIDATE_URL') 
            AND Configuration::deleteByName('PEACH_BRANDS') 
            AND Configuration::deleteByName('PEACH_PAYMENT_TYPE') 
            AND Configuration::deleteByName('PEACH_PAYNOW_TEXT') 
            AND Configuration::deleteByName('PEACH_PAYNOW_LOGO') 
            AND Configuration::deleteByName('PEACH_PAYNOW_ALIGN')
        );
    }

    public function getContent()
    {
        global $cookie;
        $errors = array();
        $html = '<div style="width:550px"><p style="text-align:center;"><a href="https://www.peachpayments.com" target="_blank"><img src="'.__PS_BASE_URI__.'modules/peach/secure_logo.png" alt="Peach" boreder="0" width="200" /></a></p><br />';

        /* Update configuration variables */
        if( Tools::isSubmit( 'submitPeach' ) ) {
            if( $paynow_text =  Tools::getValue( 'peach_paynow_text' ) )
            {
                Configuration::updateValue( 'PEACH_PAYNOW_TEXT', $paynow_text );
            }

            if( $paynow_logo =  Tools::getValue( 'peach_paynow_logo' ) )
            {
                Configuration::updateValue( 'PEACH_PAYNOW_LOGO', $paynow_logo );
            }

            if( $paynow_align =  Tools::getValue( 'peach_paynow_align' ) )
            {
                Configuration::updateValue( 'PEACH_PAYNOW_ALIGN', $paynow_align );
            }

            if ( ( $merchant_id = Tools::getValue( 'peach_user_id' ) ) AND preg_match('/[0-9]/', $merchant_id ) ) {
                Configuration::updateValue( 'PEACH_USER_ID', $merchant_id );
            }           
            else {
                $errors[] = '<div class="warning warn"><h3>'.$this->l( 'User ID seems to be wrong' ).'</h3></div>';
            }

            if ( ( $merchant_key = Tools::getValue( 'peach_password' ) ) AND preg_match('/[a-zA-Z0-9]/', $merchant_key ) ) {
                Configuration::updateValue( 'PEACH_PASSWORD', $merchant_key );
            }
            else {
                $errors[] = '<div class="warning warn"><h3>'.$this->l( 'Password seems to be wrong' ).'</h3></div>';
            }

            if ( ( $merchant_entity_id = Tools::getValue( 'peach_entity_id' ) ) AND preg_match('/[a-zA-Z0-9]/', $merchant_entity_id ) ) {
                Configuration::updateValue( 'PEACH_ENTITY_ID', $merchant_entity_id );
            }
            else {
                $errors[] = '<div class="warning warn"><h3>'.$this->l( 'Entity ID seems to be wrong' ).'</h3></div>';
            }

            if ( ( $merchant_access_token = Tools::getValue( 'peach_access_token' ) ) AND preg_match('/[a-zA-Z0-9]/', $merchant_access_token ) ) {
                Configuration::updateValue( 'PEACH_ACCESS_TOKEN', $merchant_access_token );
            }
            else {
                $errors[] = '<div class="warning warn"><h3>'.$this->l( 'Access Token seems to be wrong' ).'</h3></div>';
            }

            if ( ( $merchant_mode = Tools::getValue( 'peach_mode' ) ) AND preg_match('/[a-zA-Z0-9]/', $merchant_mode ) ) {
                Configuration::updateValue( 'PEACH_MODE', $merchant_mode );
            }
            else {
                $errors[] = '<div class="warning warn"><h3>'.$this->l( 'Mode seems to be wrong' ).'</h3></div>';
            }

            foreach ( array('displayLeftColumn', 'displayRightColumn', 'displayFooter') as $hookName ) {
                if ( $this->isRegisteredInHook($hookName) ) {
                    $this->unregisterHook($hookName);
                }
            }
 
            if ( Tools::getValue('logo_position') == self::LEFT_COLUMN ) {
                $this->registerHook('displayLeftColumn');
            }
            else if ( Tools::getValue('logo_position') == self::RIGHT_COLUMN ) {
                $this->registerHook('displayRightColumn'); 
            }
            else if ( Tools::getValue('logo_position') == self::FOOTER ) {
                $this->registerHook('displayFooter'); 
            }

            if( method_exists ('Tools','clearSmartyCache') ) {
                Tools::clearSmartyCache();
            } 
        }      
        
        /* Display errors */
        if( sizeof($errors) )
        {
            $html .= '<ul style="color: red; font-weight: bold; margin-bottom: 30px; width: 506px; background: #FFDFDF; border: 1px dashed #BBB; padding: 10px;">';
            foreach ( $errors AS $error )
                $html .= '<li>'.$error.'</li>';
            $html .= '</ul>';
        }

        $blockPositionList = array(
            self::DISABLE => $this->l('Disable'),
            self::LEFT_COLUMN => $this->l('Left Column'),
            self::RIGHT_COLUMN => $this->l('Right Column'),
            self::FOOTER => $this->l('Footer')
        );

        if ( $this->isRegisteredInHook('displayLeftColumn') ) {
            $currentLogoBlockPosition = self::LEFT_COLUMN ;
        }
        elseif( $this->isRegisteredInHook('displayRightColumn') ) {
            $currentLogoBlockPosition = self::RIGHT_COLUMN; 
        }
        elseif( $this->isRegisteredInHook('displayFooter')) {
            $currentLogoBlockPosition = self::FOOTER;
        }
        else {
            $currentLogoBlockPosition = -1;
        }
        
        /* Display settings form */
        $html .= '
        <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
          <fieldset>
          <legend><img src="'.__PS_BASE_URI__.'modules/peach/logo.png" width="28" />'.$this->l('Settings').'</legend>
            <p>'.$this->l('Use the "Test" mode to test out the module then you can use the "Live" mode if no problems arise.').'</p>
            <label>
              '.$this->l('Mode').'
            </label>
            <div class="margin-form" style="width:110px;">
              <select name="peach_mode">
                <option value="live"'.(Configuration::get('PEACH_MODE') == 'live' ? ' selected="selected"' : '').'>'.$this->l('Live').'&nbsp;&nbsp;</option>
                <option value="test"'.(Configuration::get('PEACH_MODE') == 'test' ? ' selected="selected"' : '').'>'.$this->l('Test').'&nbsp;&nbsp;</option>
              </select>
            </div>

            <p>'.$this->l('You can find your User ID and Password in your Peach account > My Account > Integration.').'</p>

            <label>
              '.$this->l('User ID').'
            </label>
            <div class="margin-form">
              <input type="text" name="peach_user_id" value="'.Tools::getValue('peach_user_id', Configuration::get('PEACH_USER_ID')).'" />
            </div>

            <label>
              '.$this->l('Password').'
            </label>
            <div class="margin-form">
              <input type="text" name="peach_password" value="'.trim(Tools::getValue('peach_password', Configuration::get('PEACH_PASSWORD'))).'" />
            </div>

            <label>
              '.$this->l('Entity ID').'
            </label>
            <div class="margin-form">
              <input type="text" name="peach_entity_id" value="'.trim(Tools::getValue('peach_entity_id', Configuration::get('PEACH_ENTITY_ID'))).'" />
            </div>

            <label>
              '.$this->l('Access Token').'
            </label>
            <div class="margin-form">
              <input type="text" name="peach_access_token" value="'.trim(Tools::getValue('peach_access_token', Configuration::get('PEACH_ACCESS_TOKEN'))).'" />
            </div>

            <p>'.$this->l('During checkout the following is what the client gets to click on to pay with Peach.').'</p>';

        //Pay now text field
        $html .= '<label>
                    '.$this->l('PayNow Text').'
                  </label>
                  <div class="margin-form" style="margin-top:5px">
                    <input type="text" name="peach_paynow_text" value="'. Configuration::get('PEACH_PAYNOW_TEXT').'">
                  </div>';

        //Pay Now text preview.
        $html .= '<label>Text Preview</label>
                  <div>
                    '.Configuration::get('PEACH_PAYNOW_TEXT') .
                    '&nbsp&nbsp<img alt="Pay Now With Peach" title="Pay Now With Peach" src="'.__PS_BASE_URI__.'modules/peach/logo.png" width="18">
                  </div>';

        //image position field
        $html .= '<p>'.$this->l('Where would you like the the Secure Payments made with Peach image to appear on your website?').'</p>
            <label>
            '.$this->l('Select the image position').'
            <label>
            <div class="margin-form" style="margin-bottom:18px;width:110px;">
                  <select name="logo_position">';
        foreach($blockPositionList as $position => $translation)
        {
            $selected = ($currentLogoBlockPosition == $position) ? 'selected="selected"' : '';
            $html .= '<option value="'.$position.'" '.$selected.'>'.$translation.'</option>';
        }
        $html .='</select></div>

            <div style="float:right;"><input type="submit" name="submitPeach" class="button" value="'.$this->l('   Save   ').'" /></div><div class="clear"></div>
          </fieldset>
        </form>
        <br /><br />
        <fieldset>
          <legend><img src="../img/admin/warning.gif" />'.$this->l('Information').'</legend>
          <p>- '.$this->l('In order to use your Peach module, you must insert your Peach User ID and Password above.').'</p>
          <p>- '.$this->l('Any orders in currencies other than ZAR will be converted by prestashop prior to be sent to the Peach payment gateway.').'<p>
          <p>- '.$this->l('It is possible to setup an automatic currency rate update using crontab. You will simply have to create a cron job with currency update link available at the bottom of "Currencies" section.').'<p>
        </fieldset>
        </div>';

        return $html;
    }

    private function _displayLogoBlock( $position )
    {      
        return '<div style="text-align:center;"><a href="https://www.peachpayments.com" target="_blank" title="Secure Payments With Peach"><img src="'.__PS_BASE_URI__.'modules/peach/secure_logo.png" width="150" /></a></div>';
    }

    public function hookDisplayRightColumn( $params )
    {
        return $this->_displayLogoBlock(self::RIGHT_COLUMN);
    }

    public function hookDisplayLeftColumn( $params )
    {
        return $this->_displayLogoBlock(self::LEFT_COLUMN);
    }  

    public function hookDisplayFooter( $params )
    {
        $html = '<section id="peach_footer_link" class="footer-block col-xs-12 col-sm-2">        
        <div style="text-align:center;"><a href="https://www.peachpayments.com" target="_blank" title="Secure Payments With Peach"><img src="'.__PS_BASE_URI__.'modules/peach/secure_logo.png"  /></a></div>  
        </section>';
        return $html;
    }    

    public function hookPaymentOptions( $params )
    {
        if(!$this->active) {
            return;
        }

        $payment_options = [
            $this->getCardPaymentOption()
        ];

        return $payment_options;
        
    }

    public function getCardPaymentOption()
    {   
        global $cookie, $cart;

        $currency_code = 'ZAR';
      
        // Buyer details
        $customer = new Customer((int)($cart->id_customer));
        
        $toCurrency = new Currency(Currency::getIdByIsoCode($currency_code));
        $fromCurrency = new Currency((int)$cookie->id_currency);
        
        $total = $cart->getOrderTotal();

        $the_amount = Tools::convertPriceFull( $total, $fromCurrency, $toCurrency );
       
        $data = array();

        $currency = $this->getCurrency((int)$cart->id_currency);
        if( $cart->id_currency != $currency->id )
        {
            $cart->id_currency = (int)$currency->id;
            $cookie->id_currency = (int)$cart->id_currency;
            $cart->update();
        }
        
        //$data['info']['authentication.userId'] = Configuration::get('PEACH_USER_ID');
        //$data['info']['authentication.password'] = Configuration::get('PEACH_PASSWORD');
        $data['info']['entityId'] = Configuration::get('PEACH_ENTITY_ID');

        $data['peach_paynow_text'] = Configuration::get('PEACH_PAYNOW_TEXT');        
        $data['peach_paynow_logo'] = Configuration::get('PEACH_PAYNOW_LOGO');      
        $data['peach_paynow_align'] = Configuration::get('PEACH_PAYNOW_ALIGN');

        // Create URLs
        //$data['info']['return_url'] = $this->context->link->getPageLink( 'order-confirmation', null, null, 'key='.$cart->secure_key.'&id_cart='.(int)($cart->id).'&id_module='.(int)($this->id));
        //$data['info']['cancel_url'] = Tools::getHttpHost( true ).__PS_BASE_URI__;
        //$data['info']['notify_url'] = Tools::getHttpHost( true ).__PS_BASE_URI__.'modules/peach/validation.php?itn_request=true';
    
        $data['info']['amount'] = number_format(sprintf( "%01.2f", $the_amount), 2, '.', '');
        $data['info']['currency'] = $currency_code;
        $data['info']['paymentType'] = 'DB';
            
        $peach_output = '';

        // Create output string
        foreach (($data['info']) as $key => $val) {
            $peach_output .= $key .'='. urlencode( trim( $val ) ) .'&';
        }

        $prepare_url = Configuration::get('PEACH_LIVE_PREPARE_URL');
        if (Configuration::get('PEACH_MODE') == 'test') {
            $prepare_url = Configuration::get('PEACH_TEST_PREPARE_URL');
        }

        // Do some cURL.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $prepare_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('Authorization:Bearer %s', Configuration::get('PEACH_ACCESS_TOKEN'))));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $peach_output);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            print '<pre>Error: ';
            print_r(curl_error($ch));
            print '</pre>';
        }

        curl_close($ch);

        $checkout_data = json_decode($responseData);

        //$action = $this->context->link->getPageLink( 'order-confirmation', null, null, 'key='.$cart->secure_key.'&id_cart='.(int)($cart->id).'&id_module='.(int)($this->id));
        $action = $this->context->link->getModuleLink($this->name, 'validation', array('checkout_id' => $checkout_data->id), true);

        $checkout_url = Configuration::get('PEACH_LIVE_CHECKOUT_URL');
        if (Configuration::get('PEACH_MODE') == 'test') {
            $checkout_url = Configuration::get('PEACH_TEST_CHECKOUT_URL');
        }

        $this->context->smarty->assign([
            'peach_action' => $action,
            'peach_checkout_url' => str_replace('{checkout_id}', $checkout_data->id, $checkout_url),
            'peach_brands' => Configuration::get('PEACH_BRANDS'),
        ]);

        // create the payment option object
        $externalOption = new PaymentOption();
        $externalOption->setCallToActionText($this->l(Configuration::get('PEACH_PAYNOW_TEXT')))
            ->setForm($this->context->smarty->fetch('module:peach/peach_form.tpl'))
            ->setAdditionalInformation($this->context->smarty->fetch('module:peach/payment_info.tpl'))
            ->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/logo_small.png'))
            ->setModuleName($this->name);
                        
        return $externalOption;
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active) {
            return;
        }

        $test = __FILE__;

        return $this->display($test, 'peach_success.tpl'); 
    }
   
}
