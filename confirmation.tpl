{* Copyright (c) 2018 Plaas *}

{if $status == 'ok'}
    <p>{l s='Your order on' mod='peach'} <span class="bold">{$shop_name}</span> {l s='is complete.' mod='peach'}
        <br /><br /><span class="bold">{l s='Your order will be shipped as soon as possible.' mod='peach'}</span>
        <br /><br />{l s='For any questions or for further information, please contact our' mod='peach'} <a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='peach'}</a>.
    </p>
{else}
    {if $status == 'pending'}
        <p>{l s='Your order on' mod='peach'} <span class="bold">{$shop_name}</span> {l s='is pending.' mod='peach'}
            <br /><br /><span class="bold">{l s='Your order will be shipped as soon as we receive your bankwire.' mod='peach'}</span>
            <br /><br />{l s='For any questions or for further information, please contact our' mod='peach'} <a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='peach'}</a>.
        </p>
    {else}
        <p class="warning">
            {l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='peach'} 
            <a href="{$link->getPageLink('contact', true)}">{l s='customer support' mod='peach'}</a>.
        </p>
    {/if}
{/if}
