{* Copyright (c) 2018 Plaas *}

<div class='peachPayNow'>
	<form id='peachPayNow' action="{$data.peach_url}" method="post">
	    <p class="payment_module">
		    {foreach $data.info as $k=>$v}
		        <input type="hidden" name="{$k}" value="{$v}" />
		    {/foreach}

		     <a href='#' onclick='document.getElementById("peachPayNow").submit();return false;'>
		     	{$data.peach_paynow_text}

		     	{if $data.peach_paynow_logo=='on'} 
		     	<img align='{$data.payfast_paynow_align}' alt='Pay Now With Peach' title='Pay Now With Peach' src="{$base_dir}modules/peach/logo.png">
		        {/if}
		    </a>
		    
		    <noscript>
		    	<input type="image" src="{$base_dir}modules/peach/logo.png">
		    </noscript>
	    </p> 
	</form>
</div>

<div class="clear"></div>